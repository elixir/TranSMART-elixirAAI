# TranSMART integration with ELIXIR AAI
The code and all the information is at [https://git-r3lab.uni.lu/elixir/AAI-proxy](https://git-r3lab.uni.lu/elixir/AAI-proxy).
If you'd like to use it, please contact `jacek.lebioda@elixir-luxembourg.org`.